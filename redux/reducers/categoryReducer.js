export default (state = {}, action) => {
  switch (action.type) {
    case "replace":
      console.log(action);
      return {
        ...state,
        categories: action.payload
      };
    case "add":
      var updatedState = [...state.categories];
      updatedState.push(action.payload);
      return {
        ...state,
        categories: updatedState
      };
    case "remove":
      var updatedState = [...state.categories];
      var categoryId = parseInt(action.payload);
      var idx = updatedState.findIndex((e, idx, array) => {
        if (e.category_id === categoryId) return true;
        return false;
      });
      if (idx !== -1) {
        updatedState.splice(idx, 1);
      }
      return {
        ...state,
        categories: updatedState
      };

    default:
      return state;
  }
  return state;
};
