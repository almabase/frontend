export default (state = { categoryData: {} }, action) => {
  var categoryId;
  var pokemons;
  var pokemon;
  switch (action.type) {
    case "replaceCategory":
      categoryId = action.payload.category;
      pokemons = action.payload.pokemons;
      return {
        ...state,
        categoryData: {
          ...state.categoryData,
          [categoryId]: pokemons
        }
      };
    case "addPokemonToCategory":
      var updatedState = { ...state.categoryData };
      categoryId = action.payload.category;
      pokemon = action.payload.pokemon;

      if (categoryId in updatedState) {
        updatedState[categoryId].push(pokemon);
      }

      return {
        ...state,
        categoryData: updatedState
      };
    case "removePokemonFromCategory":
      var updatedState = [...state.categoryData];
      categoryId = action.payload.category;
      pokemon = action.payload.pokemon;
      if (categoryId in updatedState) {
        var idx = updatedState[categoryId].findIndex((e, idx, array) => {
          if (e.id === pokemonId) return true;
          return false;
        });
        if (idx !== -1) {
          updatedState[categoryId].splice(idx, 1);
        }
      }

      return {
        ...state,
        categoryData: updatedState
      };

    default:
      return state;
  }
  return state;
};
