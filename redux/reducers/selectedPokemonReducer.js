export default (state = { selectedPokemon: [] }, action) => {
  switch (action.type) {
    case "addPokemon":
      var updatedState = [...state.selectedPokemon];
      updatedState.push(action.payload);
      return {
        ...state,
        selectedPokemon: updatedState
      };
    case "removePokemon":
      var updatedState = [...state.selectedPokemon];
      let idx = updatedState.indexOf(action.payload);
      if (idx !== -1) {
        updatedState.splice(idx, 1);
      }
      return {
        ...state,
        selectedPokemon: updatedState
      };
    case "removeAllPokemon":
      return {
        selectedPokemon: []
      };
    default:
      return state;
  }
  return state;
};
