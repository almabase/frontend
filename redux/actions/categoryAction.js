const doAction = (type, payload) => {
  return {
    type: type,
    payload
  };
};

export default doAction;
