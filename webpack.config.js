const path = require("path");
const HWP = require("html-webpack-plugin");
module.exports = {
  entry: path.join(__dirname, "/src/index.js"),
  output: {
    filename: "build.js",
    path: path.join(__dirname, "/dist")
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader", // Creates `style` nodes from JS strings
          "css-loader", // Translates CSS into CommonJS
          "sass-loader" // Compiles Sass to CSS
        ]
      },
      { test: /\.css$/, loader: "style-loader!css-loader" },
      {
        test: /\.svg$/,
        loader: "svg-inline-loader"
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: "file-loader"
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/"
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      views: path.resolve(__dirname, "views/"),
      src: path.resolve(__dirname, "src/"),
      css: path.resolve(__dirname, "css/"),
      images: path.resolve(__dirname, "images/"),
      config: path.resolve(__dirname, "src/config/"),
      reducers: path.resolve(__dirname, "redux/reducers/"),
      actions: path.resolve(__dirname, "redux/actions/")
    }
  },
  plugins: [new HWP({ template: path.join(__dirname, "/src/index.html") })]
};
