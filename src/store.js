import { createStore } from "redux";
import { combineReducers } from "redux";
import { sessionReducer as session } from "redux-react-session";
import { sessionService } from "redux-react-session";

import categoryReducer from "reducers/categoryReducer";
import selectedPokemonReducer from "reducers/selectedPokemonReducer";
import pokemonReducer from "reducers/pokemonReducer";

const reducers = {
  categoryReducer: categoryReducer,
  selectedPokemonReducer: selectedPokemonReducer,
  pokemonReducer: pokemonReducer,
  session: session
};
const reducer = combineReducers(reducers);
const store = createStore(reducer);
sessionService.initSessionService(store);

export default store;
