import axios from "axios";
const setAuthToken = token => {
  if (token) {
    // Apply authorization token to every request if logged in
    axios.defaults.headers.common["Authorization"] = "Token " + token;
  } else {
    // Delete auth header
    delete axios.defaults.headers.common["Authorization"];
  }
};

const getAuthToken = () => {
  try {
    return axios.defaults.headers.common["Authorization"];
  } catch (e) {
    return null;
  }
}
export default setAuthToken;
export { getAuthToken };
