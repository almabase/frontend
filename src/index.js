import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { Spinner } from "reactstrap";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  withRouter
} from "react-router-dom";
import Notfound from "views/404";
import HomePage from "views/HomePage";
import store from "./store";
import { Provider } from "react-redux";

import "bootstrap/dist/css/bootstrap.css";
import "react-toastify/dist/ReactToastify.min.css";
import "css/common.scss";
require("typeface-roboto");

const suspense = <Spinner style={{ width: "3rem", height: "3rem" }} />;

const routing = (
  <Suspense fallback={suspense}>
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/" component={withRouter(HomePage)} />
          <Route component={Notfound} />
        </Switch>
      </Router>
    </Provider>
  </Suspense>
);

ReactDOM.render(routing, document.getElementById("root"));
