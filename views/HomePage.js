import React, { useState, useEffect } from "react";
import { Container, Row, Col, Card, CardBody, Input } from "reactstrap";
import Tabs from "views/components/Tabs";
import CategoryData from "views/components/CategoryData";
import { Scrollbars } from "react-custom-scrollbars";
import { ToastContainer, toast } from "react-toastify";
import LoginCategory from "views/components/LoginCategory";
import { sessionService } from "redux-react-session";
import axios from "axios";
import api_url from "config/config";

import setAuthToken from "config/auth";

import logo from "images/pokedex.png";

function HomePage(props) {
  const [activeTab, setActiveTab] = useState("0");
  const [activeTabName, setActiveTabName] = useState("all");
  const [refreshData, setRefreshData] = useState(false);
  const [showLogin, setShowLogin] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  const [searchText, setSearchText] = useState("");

  /**
   * Save Token from Session and append to Axios
   */
  useEffect(() => {
    sessionService
      .loadSession()
      .then(currentSession => {
        setRefreshData(true);
        setAuthToken(currentSession.token);
        if (currentSession.token !== undefined) {
          setIsLogin(true);
        }
      })
      .catch(err => console.log(err));
  }, []);

  /**
   * Change Tab
   * @param {String} tab TabIndex
   * @param {String} tabName TabName
   */
  const toggleTabs = (tab, tabName) => {
    if (activeTab !== tab) {
      setActiveTab(tab);
      setActiveTabName(tabName);
    }
  };

  /**
   * Toggle Login Modal
   */
  const toggleLogin = () => {
    if (isLogin) {
      axios({
        method: "GET",
        url: api_url + "logout/token/",
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(function(response) {
        setLogin(false);
      })
      .catch(function(error) {
        toast.error("Unable to log out. Try again.");
      });
    } else {
      setShowLogin(!showLogin);
    }
  };

  const setLogin = (loginStatus) => {
    setIsLogin(loginStatus);
    setRefreshData(true);
    if (!loginStatus) {
      setShowLogin(loginStatus);
      setAuthToken(null);
      sessionService.deleteSession();
    }
  }

  const changeSearch = e => {
    setSearchText(e.target.value);
  };

  return (
    <Container fluid>
      <Row>
        <a href="#" onClick={toggleLogin}>
          {isLogin === true ? "Logout" : "Login"}
        </a>
        <LoginCategory toggle={toggleLogin} show={showLogin} setLogin={setLogin} />
      </Row>
      <Row style={{ marginTop: "28px" }}>
        <Col className="text-center">
          <img
            style={{ maxWidth: "100%", minHeight: "100%" }}
            src={logo}
            className="rounded"
            alt="Logo"
          />
        </Col>
      </Row>
      <Row>
        <Col md={{ offset: 2, size: 8 }}>
          <Card className="shadow m-t-34" style={{ height: "74.4vh" }}>
            <Tabs
              toggle={(tab, tabName) => toggleTabs(tab, tabName)}
              activeTab={activeTab}
              refreshData={refreshData}
              setRefreshData={value => setRefreshData(value)}
              setLogin={setLogin}
            />
            {activeTab === "0" && (
              <Input
                type="text"
                name="pokemon_name"
                id="pokemon_name"
                placeholder="Search Pokemon"
                className="pb-4 pt-4 display-4"
                value={searchText}
                onChange={changeSearch}
              />
            )}
            <Scrollbars
              style={{ width: "100%" }}
              universal={true}
              autoHide={true}
              style={{ borderBottom: "1px solid #dee2e6" }}
            >
              <CardBody>
                <CategoryData
                  categoryId={activeTab}
                  categoryName={activeTabName}
                  searchText={searchText}
                />
              </CardBody>
            </Scrollbars>
          </Card>
        </Col>
      </Row>
      <ToastContainer />
    </Container>
  );
}

export default HomePage;
