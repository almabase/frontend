import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import doAction from "actions/categoryAction";
import { connect } from "react-redux";
import axios from "axios";

import api_url from "config/config";

const DeleteCategoryModal = props => {
  const { className, toggle, show } = props;
  const { categoryId, categoryName, count } = props;

  /**
   * Delete Category
   */
  const removeCategory = () => {
    axios({
      method: "DELETE",
      url: api_url + "api/user_category/" + categoryId + "/"
    })
      .then(response => {
        props.removeCategory(categoryId);
        toggle();
      })
      .catch(err => {
        props.removeCategory(categoryId);
        toggle();
      });
  };

  return (
    <div>
      <Modal
        isOpen={show}
        toggle={toggle}
        className={className}
        backdrop={true}
      >
        <ModalHeader toggle={toggle}>Remove Category</ModalHeader>
        <ModalBody>
          <b>Are you sure you want to delete '{categoryName}'?</b>
          <br />
          All {count} Pokemon(s) in this category will be deleted as well.
        </ModalBody>
        <ModalFooter>
          <Button
            className="rounded-button"
            onClick={removeCategory}
            style={{ backgroundColor: "#E03C3C" }}
          >
            Delete
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  removeCategory: category => dispatch(doAction("remove", category))
});

export default connect(null, mapDispatchToProps)(DeleteCategoryModal);
