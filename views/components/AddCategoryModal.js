import React, { useState, useEffect } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import CreatableSelect from "react-select/creatable";
import doAction from "actions/categoryAction";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import axios from "axios";

import api_url from "config/config";

const AddCategoryModal = props => {
  const { className, toggle, show } = props;
  const { categories, selectedPokemon, categoryData } = props;
  const [label, setLabel] = useState([]);
  var isNewCategory = false;
  var categoryValue = null;

  /**
   * Add Categories to dropdown
   */
  useEffect(() => {
    const newLabels = [];
    categories.map((category, idx) => {
      if (category.category_id === "0") return;
      newLabels.push({
        value: category.category_id,
        label: category.category_name
      });

      return;
    });
    setLabel(newLabels);
  }, []);

  /**
   * Invoked when any change is performed in dropdown
   * @param {string} newValue Value of Category
   * @param {object} actionMeta Action performed (create/select/clear)
   */
  const handleChange = (newValue, actionMeta) => {
    if (actionMeta.action === "create-option") {
      isNewCategory = true;
      categoryValue = newValue.value;
    } else if (actionMeta.action === "select-option") {
      isNewCategory = false;
      categoryValue = newValue.value;
    } else {
      // clear
      isNewCategory = false;
      categoryValue = null;
    }
  };

  const handleInputChange = (inputValue, actionMeta) => {
    // do nothing
  };

  /**
   * Add New Category to DB
   */
  const addCategory = () => {
    axios({
      method: "POST",
      url: api_url + "api/user_category/",
      data: {
        category_name: categoryValue
      },
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        props.addCategory(response.data);
        addPokemons(response.data.category_id);
      })
      .catch(err => {
        toast.error("Cannot save new category, please try again later");
      });
  };

  /**
   * Add Pokemons selected to specific category
   * @param {integer} categoryId CategoryID
   */
  const addPokemons = categoryId => {
    var isError = false;
    selectedPokemon.forEach(pokemonId => {
      axios({
        method: "POST",
        data: {
          category: categoryId,
          pokemon: pokemonId,
          order: -1
        },
        url: api_url + "api/pokemon_category/"
      })
        .then(function(response) {
          props.removePokemon(response.data.pokemon);
          var idx = categoryData[0].findIndex((e, idx, array) => {
            if (e.id === pokemonId) return true;
            return false;
          });
          if (idx !== -1) {
            props.addPokemonToCategory(categoryId, categoryData[0][idx]);
          } else {
            toast.info("Please refresh to see the changes");
          }
        })
        .catch(function(error) {
          console.log(error);
          isError = true;
          if (isError) {
            toast.error("Cannot add some/all pokemons to category.");
          }
        });
    });
  };

  /**
   * Called when "Save" button is clicked
   */
  const save = () => {
    if (categoryValue === null) {
      toast.error("Category is not selected");
      return;
    } else if (isNewCategory) {
      addCategory();
    } else {
      addPokemons(categoryValue);
    }
    toggle();
  };

  return (
    <div>
      <Modal
        isOpen={show}
        toggle={toggle}
        className={className}
        backdrop={true}
      >
        <ModalHeader toggle={toggle}>Add to Category</ModalHeader>
        <ModalBody>
          Select or Create New Category:
          <CreatableSelect
            isClearable
            onChange={handleChange}
            onInputChange={handleInputChange}
            options={label}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            className="rounded-button"
            onClick={save}
            styles={{ backgroundColor: "#1B1A80", color: "#ffffff" }}
          >
            Save
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

const mapStateToProps = state => ({
  categories: state.categoryReducer.categories,
  selectedPokemon: state.selectedPokemonReducer.selectedPokemon,
  categoryData: state.pokemonReducer.categoryData
});

const mapDispatchToProps = dispatch => ({
  addCategory: category => dispatch(doAction("add", category)),
  removeCategory: category => dispatch(doAction("remove", category)),
  replaceCategory: category => dispatch(doAction("replace", category)),
  removeAllPokemon: () => dispatch(doAction("removeAllPokemon")),
  removePokemon: pokemon => dispatch(doAction("removePokemon", pokemon)),
  addPokemonToCategory: (category, pokemon) =>
    dispatch(
      doAction("addPokemonToCategory", { category: category, pokemon: pokemon })
    )
});

export default connect(mapStateToProps, mapDispatchToProps)(AddCategoryModal);
