import React, { useEffect, useState } from "react";
import { Spinner, Row, Col } from "reactstrap";
import PokemonRow from "views/components/PokemonRow";
import AddCategoryModal from "views/components/AddCategoryModal";
import DeleteCategoryModal from "views/components/DeleteCategoryModal";

import axios from "axios";
import doAction from "actions/categoryAction";
import { connect } from "react-redux";

import api_url from "config/config";
import { LazyLoadComponent } from "react-lazy-load-image-component";
import { Container, Button, Link } from "react-floating-action-button";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

function CategoryData(props) {
  const {
    categoryId,
    selectedPokemon,
    categoryData,
    categoryName,
    searchText
  } = props;
  const [isRequesting, setIsRequesting] = useState({});
  const [refreshState, setRefreshState] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [undoOrder, setUndoOrder] = useState({});

  /**
   * Set if API request is initiated
   * @param {String} catId CategoryID
   * @param {boolean} value true/false
   */
  const setRequesting = (catId, value) => {
    setIsRequesting(prevState => ({
      ...prevState,
      [catId]: value
    }));
  };

  /**
   * Call API to get CategoryData(Pokemons)
   */
  const getData = () => {
    var catId = categoryId;
    if (catId in isRequesting && isRequesting[catId] === true) return;

    setRequesting(catId, true);

    axios
      .get(api_url + "api/pokedex/")
      .then(function(response) {
        // Replace Category with Pokemons
        props.replaceCategory(catId, response.data);
        setRequesting(catId, false);
      })
      .catch(function(error) {
        setRequesting(catId, false);
        setTimeout(() => {
          getData();
        }, 5000);
      });
  };

  /**
   * Update ordering of pokemons in category
   * @param {integer} uid UniqueID
   * @param {integer} order Ordering
   */
  const updateOrder = (uid, order) => {
    axios({
      method: "patch",
      url: api_url + "api/pokemon_category/" + uid + "/",
      data: {
        order: order
      }
    })
      .then(function(response) {})
      .catch(function(error) {
        toast.error("Cannot Update");
      });
  };

  /**
   * Get Pokemon Data of specific category
   */
  const getCategoryData = () => {
    var catId = categoryId;
    if (catId in isRequesting && isRequesting[catId] === true) return;

    setRequesting(catId, true);

    axios({
      method: "get",
      url: api_url + "api/pokemon_category/" + catId + "/"
    })
      .then(function(response) {
        if (response.data) {
          var pokemon = [];
          // Update pokemons and replace category
          response.data.map((data, idx) => {
            let tempVar = data.pokemon;
            tempVar["order"] = data.order;
            tempVar["uid"] = data.id;
            pokemon.push(tempVar);
            return;
          });
          props.replaceCategory(catId, pokemon);
          setRequesting(catId, false);
        }
      })
      .catch(function(error) {
        setRequesting(catId, false);
        setTimeout(() => {
          getCategoryData();
        }, 5000);
      });
  };

  /**
   * On First Load, get Data("all category") or call getCategoryData("specific category")
   */
  useEffect(() => {
    if (categoryId in categoryData && categoryData[categoryId] != null) return;
    if (categoryId === "0") {
      getData();
    } else {
      getCategoryData();
    }
  }, [categoryId]);

  /**
   * invoked when checkbox is selected
   * @param {integer} pokemonID PokemonID
   * @param {boolean} removeItem Remove or Add Selection
   */
  const setSelectedID = (pokemonID, removeItem) => {
    if (removeItem) {
      props.removePokemon(pokemonID);
    } else {
      props.addPokemon(pokemonID);
    }
  };

  /**
   * Toggle Modal
   */
  const toggleModal = () => {
    setShowModal(!showModal);
    setRefreshState(!refreshState);
  };

  /**
   * Add Drag/Drop (Reordering) state
   * @param {integer} startIndex Start Index of Pokemon
   * @param {integer} endIndex End(Dropped) index of pokemon
   */
  const addUndoOrder = (startIndex, endIndex) => {
    var undoOrderTemp = [];
    if (categoryId in undoOrder) {
      undoOrderTemp = [...undoOrder[categoryId]];
    }
    undoOrderTemp.push([startIndex, endIndex]);
    setUndoOrder(prevState => ({
      ...prevState,
      [categoryId]: undoOrderTemp
    }));
  };

  /**
   * Undo last reordering
   */
  const undo = () => {
    if (undoOrder[categoryId].length <= 0) return;
    var undoOrderTemp = [];
    if (categoryId in undoOrder) {
      undoOrderTemp = [...undoOrder[categoryId]];
    }
    let indexes = undoOrderTemp.pop();
    setUndoOrder(prevState => ({
      ...prevState,
      [categoryId]: undoOrderTemp
    }));
    dragComplete(indexes[1], indexes[0]);
  };

  /**
   * Initiate Reorder. Reorders the list
   * @param {array} list List of Data
   * @param {integer} startIndex Start Index
   * @param {integer} endIndex End Index
   */
  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    for (var i = 0; i < result.length; i++) {
      result[i].order = i;
      updateOrder(result[i].uid, result[i].order);
    }

    return result;
  };

  /**
   * Dragging element complete: Reorder the list and update to server
   */
  const dragComplete = (startIndex, endIndex) => {
    var catData = [...categoryData[categoryId]];

    const items = reorder(catData, startIndex, endIndex);

    props.replaceCategory(categoryId, items);
  };

  /**
   * Dragging completed
   * @param {object} result Result object of source and destination information
   */
  const onDragEnd = result => {
    // dropped outside the list
    if (!result.destination) return;
    if (result.source.index === result.destination.index) return;

    dragComplete(result.source.index, result.destination.index);
    addUndoOrder(result.source.index, result.destination.index);
  };

  const grid = 8;

  /** Style */
  const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: "none",
    margin: `0 0 ${grid}px 0`,

    background: isDragging ? "lightgreen" : "transparent",

    ...draggableStyle
  });

  /** Style */
  const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "lightblue" : "transparent"
  });

  if (
    categoryData &&
    categoryId in categoryData &&
    categoryData[categoryId] != null
  ) {
    return (
      <>
        {undoOrder &&
          categoryId in undoOrder &&
          undoOrder[categoryId].length > 0 && (
            <a href="#" onClick={undo} style={{ color: "##33309A" }}>
              Undo Reorder
            </a>
          )}
        <LazyLoadComponent>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  style={getListStyle(snapshot.isDraggingOver)}
                >
                  {categoryData[categoryId].map((pokemon, idx) => {
                    if (categoryId !== "0") {
                      return (
                        <Draggable
                          key={"P" + idx}
                          draggableId={"P" + idx.toString()}
                          index={idx}
                        >
                          {(provided1, snapshot1) => (
                            <div
                              ref={provided1.innerRef}
                              {...provided1.draggableProps}
                              {...provided1.dragHandleProps}
                              style={getItemStyle(
                                snapshot1.isDragging,
                                provided1.draggableProps.style
                              )}
                            >
                              <PokemonRow
                                categoryId={categoryId}
                                pokemonData={pokemon}
                                setSelectedID={(id, remove) => {
                                  setSelectedID(id, remove);
                                  setRefreshState(prevState => ({
                                    refreshState: !prevState.refreshState
                                  }));
                                }}
                                selectedPokemon={selectedPokemon}
                              />
                            </div>
                          )}
                        </Draggable>
                      );
                    } else {
                      if (
                        searchText !== "" &&
                        !pokemon.name
                          .toLowerCase()
                          .includes(searchText.toLowerCase())
                      )
                        return;
                      return (
                        <PokemonRow
                          key={idx}
                          categoryId={categoryId}
                          pokemonData={pokemon}
                          setSelectedID={(id, remove) => {
                            setSelectedID(id, remove);
                            setRefreshState(prevState => ({
                              refreshState: !prevState.refreshState
                            }));
                          }}
                          selectedPokemon={selectedPokemon}
                        />
                      );
                    }
                  })}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </LazyLoadComponent>
        {selectedPokemon && selectedPokemon.length > 0 && categoryId === "0" && (
          <>
            <Container>
              <Button
                className="rounded-button"
                onClick={toggleModal}
                styles={{ backgroundColor: "#1B1A80", color: "#ffffff" }}
              >
                Add to Category
              </Button>
            </Container>
            <AddCategoryModal toggle={toggleModal} show={showModal} />
          </>
        )}
        {categoryId !== "0" && (
          <>
            <Container>
              <Button
                className="rounded-button"
                onClick={toggleModal}
                styles={{ backgroundColor: "#e53935", color: "#ffffff" }}
              >
                Delete Category
              </Button>
            </Container>
            <DeleteCategoryModal
              toggle={toggleModal}
              show={showModal}
              categoryName={categoryName}
              categoryId={categoryId}
              count={categoryData[categoryId].length}
            />
          </>
        )}
      </>
    );
  } else {
    return (
      <Row>
        <Col className="text-center">
          <Spinner style={{ width: "3rem", height: "3rem" }} />
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  selectedPokemon: state.selectedPokemonReducer.selectedPokemon,
  categoryData: state.pokemonReducer.categoryData
});

const mapDispatchToProps = dispatch => ({
  addPokemon: pokemon => dispatch(doAction("addPokemon", pokemon)),
  removePokemon: pokemon => dispatch(doAction("removePokemon", pokemon)),
  //removePokemonFromCategory: pokemon => dispatch(doAction("removePokemonFromCategory", pokemon))
  //addPokemonToCategory: pokemon => dispatch(doAction("addPokemonToCategory", pokemon))
  replaceCategory: (category, pokemons) =>
    dispatch(
      doAction("replaceCategory", { category: category, pokemons: pokemons })
    )
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryData);
