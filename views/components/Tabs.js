import React, { useState, useEffect } from "react";
import classnames from "classnames";
import { Scrollbars } from "react-custom-scrollbars";
import { Nav, NavItem, NavLink } from "reactstrap";
import axios from "axios";
import api_url from "config/config";
import doAction from "actions/categoryAction";
import { connect } from "react-redux";
import { getAuthToken } from "config/auth";

function Tabs(props) {
  const { activeTab, refreshData, setRefreshData, categories, setLogin } = props;

  const [isRequesting, setIsRequesting] = useState(0);

  // Common Category
  const tempCategory = [{ category_name: "all", category_id: "0" }];

  /**
   * Add categories on first load
   */
  useEffect(() => {
    props.replaceCategory(tempCategory);
    if (isRequesting === 0 || refreshData) {
      getData();
      if (refreshData) setRefreshData(false);
    }
  }, [refreshData]);

  /**
   * If category is deleted, switch to common tab(all)
   * If category is added, update.
   */
  useEffect(() => {
    if (categories === undefined || categories === null) return;
    var idx = categories.findIndex((e, idx, array) => {
      if (e.category_id === activeTab) return true;
      return false;
    });
    if (idx === -1) {
      props.toggle("0", "all");
    }
  }, [categories]);

  /**
   * Get Category Names (Tabs)
   */
  const getData = () => {
    if (isRequesting !== 0) return;
    if (!getAuthToken()) return;
    setIsRequesting(1);

    axios({
      method: "get",
      url: api_url + "api/user_category/"
    })
      .then(function(response) {
        var newCategories = response.data;
        props.replaceCategory(tempCategory.concat(newCategories));
        setIsRequesting(2);
      })
      .catch(function(error) {
        setIsRequesting(0);
        setTimeout(() => {
          if (error && error.response && error.response.status === 401 && error.response.data.detail === "Invalid token.") {
            //toast.warn("Invalid Token. Logging out");
            setLogin(false);
          } else {
            getData();
          }
        }, 1000);
      });
  };

  return (
    <Scrollbars
      autoHeight
      style={{ width: "100%" }}
      universal={true}
      autoHide={true}
      style={{ borderBottom: "1px solid #dee2e6", minHeight: "40px" }}
    >
      <Nav tabs style={{ borderBottom: "none" }}>
        {categories &&
          categories.length > 0 &&
          categories.map((category, idx) => {
            return (
              <NavItem key={idx}>
                <NavLink
                  className={classnames({
                    active: activeTab === category.category_id.toString()
                  })}
                  onClick={() => {
                    props.toggle(
                      category.category_id.toString(),
                      category.category_name
                    );
                  }}
                >
                  {category.category_name}
                </NavLink>
              </NavItem>
            );
          })}
      </Nav>
    </Scrollbars>
  );
}

const mapStateToProps = state => ({
  categories: state.categoryReducer.categories
});

const mapDispatchToProps = dispatch => ({
  addCategory: category => dispatch(doAction("add", category)),
  removeCategory: category => dispatch(doAction("remove", category)),
  replaceCategory: category => dispatch(doAction("replace", category))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tabs);
