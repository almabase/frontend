import React, { useState } from "react";
import { Row, Col, CustomInput } from "reactstrap";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { GiHamburgerMenu } from "react-icons/gi";

function PokemonRow(props) {
  const { categoryId, pokemonData, selectedPokemon } = props;
  const [refreshState, setRefreshState] = useState(false);

  /**
   * Toggle checkbox
   * @param {object} e Event(Checkbox)
   */
  const toggleCheckbox = e => {
    props.setSelectedID(pokemonData.id, !e.target.checked);
    setRefreshState(prevState => ({
      refreshState: !prevState.refreshState
    }));
  };
  // If no pokemonData found, return empty row
  if (!pokemonData) {
    return <Row></Row>;
  }

  var squareHeightWidth = "72px";
  if (categoryId === "0") {
    squareHeightWidth = "160px";
  }

  return (
    <Row className="border-bottom pb-2 pt-2">
      <Col sm="1" className="vertical-center">
        {categoryId === "0" && (
          <CustomInput
            addon="true"
            id={pokemonData.id}
            type="checkbox"
            aria-label="Select"
            checked={
              selectedPokemon && selectedPokemon.indexOf(pokemonData.id) === -1
                ? false
                : true
            }
            onChange={toggleCheckbox}
          />
        )}
        {categoryId !== "0" && <GiHamburgerMenu />}
      </Col>
      <div style={{ height: squareHeightWidth, width: squareHeightWidth }}>
        <LazyLoadImage
          alt={pokemonData.ThumbnailAltText}
          height={squareHeightWidth}
          src={pokemonData.ThumbnailImage}
          width={squareHeightWidth}
        />
      </div>
      {categoryId !== "0" && (
        <Col className="vertical-center font-20">{pokemonData.name}</Col>
      )}
      {categoryId === "0" && (
        <Col>
          <Row>
            <Col className="vertical-center font-20">{pokemonData.name}</Col>
          </Row>
          <Row></Row>
          <Row>
            <Col sm="6" md="6" lg="6">
              <Row>
                <Col className="font-weight-bold">Abilities</Col>
              </Row>
              <Row>
                <Col>{pokemonData.get_ability}</Col>
              </Row>
            </Col>
            <Col sm="6" md="6" lg="6">
              <Row>
                <Col className="font-weight-bold">Weakness</Col>
              </Row>
              <Row>
                <Col>{pokemonData.get_weakness}</Col>
              </Row>
            </Col>
          </Row>
        </Col>
      )}
    </Row>
  );
}

export default PokemonRow;
