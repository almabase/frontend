import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Form,
  Label,
  Input
} from "reactstrap";
import doAction from "actions/categoryAction";
import { connect } from "react-redux";
import { sessionService } from "redux-react-session";
import setAuthToken from "config/auth";
import { toast } from "react-toastify";
import axios from "axios";
import api_url from "config/config";

const LoginCategoryModal = props => {
  const { className, toggle, show, setLogin } = props;
  const [state, setState] = useState({ username: "", password: "" });

  /**
   * Login and save token
   * @param {Object} event Login Form
   */
  const login = event => {
    event.preventDefault();

    const username = state.username;
    const password = state.password;
    axios({
      method: "post",
      url: api_url + "login/token/",
      data: {
        username: username,
        password: password
      },
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(function(response) {
        if (response.data && response.data.token) {
          // Save Token
          sessionService.saveSession({ token: response.data.token });
          setAuthToken(response.data.token);
          setLogin(true);
          toast.info("Logged in");
          toggle();
        } else {
          toast.error("Invalid username/password");
        }
      })
      .catch(function(error) {
        toast.error("Cannot log in. Try again.");
      });
  };

  /**
   * Change State (Input)
   * @param {object} event Event Object
   */
  const handleChange = event => {
    const target = event.target;
    const field = target.name;
    const value = target.value;

    setState(prevState => ({
      ...prevState,
      [field]: value
    }));
  };

  return (
    <div>
      <Modal
        isOpen={show}
        toggle={toggle}
        className={className}
        backdrop={true}
      >
        <ModalHeader toggle={toggle}>Login</ModalHeader>
        <ModalBody>
          <Form id="loginForm">
            <FormGroup>
              <Label for="username">Username</Label>
              <Input
                type="text"
                validations={["required"]}
                name="username"
                value={state.username}
                onChange={handleChange}
                id="username"
                placeholder="Enter your username."
              />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input
                type="password"
                validations={["required"]}
                name="password"
                value={state.password}
                onChange={handleChange}
                id="password"
                placeholder="Enter your password."
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            className="rounded-button"
            styles={{ backgroundColor: "#1B1A80", color: "#ffffff" }}
            onClick={login}
          >
            Login
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

const mapStateToProps = state => ({
  categories: state.categories,
  session: state.session
});

const mapDispatchToProps = dispatch => ({
  addCategory: category => dispatch(doAction("add", category)),
  removeCategory: category => dispatch(doAction("remove", category)),
  replaceCategory: category => dispatch(doAction("replace", category))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginCategoryModal);
